tmuxp (1.55.0-1) unstable; urgency=medium

  [ Sébastien Delafond ]
  * New upstream version 1.55.0
  * Bump-up Standards-Version
  * Depend on python3-libtmux >= 0.46

 -- Sebastien Delafond <seb@debian.org>  Sun, 16 Mar 2025 09:41:10 +0100

tmuxp (1.54.0-1) unstable; urgency=medium

  [ Sébastien Delafond ]
  * New upstream version 1.54.0
  * Depend on python3-libtmux >= 0.45

 -- Sebastien Delafond <seb@debian.org>  Mon, 24 Feb 2025 08:28:17 +0100

tmuxp (1.52.2-1) unstable; urgency=medium

  [ Sébastien Delafond ]
  * New upstream version 1.52.2
  * Depend on python3-libtmux >= 0.42
  * Trim trailing whitespace.
  * Bump debhelper from old 12 to 13.

 -- Sebastien Delafond <seb@debian.org>  Tue, 04 Feb 2025 10:09:14 +0100

tmuxp (1.50.1-1) unstable; urgency=medium

  [ Sébastien Delafond ]
  * New upstream version 1.50.1
  * Depend on libtmux >= 0.40.1

 -- Sebastien Delafond <seb@debian.org>  Mon, 30 Dec 2024 07:43:28 +0100

tmuxp (1.49.0-1) unstable; urgency=medium

  [ Sébastien Delafond ]
  * New upstream version 1.49.0
  * Build-Depend on python3-hatchling instead of python3-poetry-core
  * Adjust versioned dependency on python3-libtmux

 -- Sebastien Delafond <seb@debian.org>  Thu, 05 Dec 2024 13:25:57 +0100

tmuxp (1.47.0-3) unstable; urgency=medium

  [ Sébastien Delafond ]
  * d/control: drop dependency on python3-pkg-resources (Closes: #1083798)
  * d/control: bump-up Standards-Version

 -- Sebastien Delafond <seb@debian.org>  Tue, 08 Oct 2024 11:34:07 +0200

tmuxp (1.47.0-2) unstable; urgency=medium

  [ Sébastien Delafond ]
  * debian/tests: skip test_environment_variables (Closes: #1060775)

 -- Sebastien Delafond <seb@debian.org>  Fri, 14 Jun 2024 11:28:48 +0200

tmuxp (1.47.0-1) unstable; urgency=medium

  [ Sébastien Delafond ]
  * New upstream version 1.47.0
  * d/control: require python3-libtmux >= 0.37.0

 -- Sebastien Delafond <seb@debian.org>  Tue, 14 May 2024 07:24:44 +0200

tmuxp (1.38.0-1) unstable; urgency=medium

  [ Sébastien Delafond ]
  * New upstream version 1.38.0
  * d/control: depend on python-libtmux >= 0.30

 -- Sebastien Delafond <seb@debian.org>  Sun, 18 Feb 2024 10:34:03 +0100

tmuxp (1.34.0-2) unstable; urgency=medium

  [ Sébastien Delafond ]
  * test_environment_variables: raise timeout from 0.3s to 3s (Closes: #1060775)

 -- Sebastien Delafond <seb@debian.org>  Wed, 17 Jan 2024 11:08:43 +0100

tmuxp (1.34.0-1) unstable; urgency=medium

  [ Sébastien Delafond ]
  * New upstream version 1.34.0

 -- Sebastien Delafond <seb@debian.org>  Wed, 27 Dec 2023 11:24:04 +0100

tmuxp (1.31.0-2) unstable; urgency=medium

  [ Sébastien Delafond ]
  * d/control: do not build-depend on python3-kaptan
  * d/control: build-depend on python3-libtmux >= 0.23.2-1~1 to assuage lintian

 -- Sebastien Delafond <seb@debian.org>  Thu, 05 Oct 2023 07:31:49 +0200

tmuxp (1.31.0-1) unstable; urgency=medium

  [ Sébastien Delafond ]
  * New upstream version 1.31.0
  * d/control: depend on python3-libtmux >= 0.23.2-1
  * d/control: build-depend on pybuild-plugin-pyproject and python3-poetry-core
  * d/control: bump-up Standards-Version
  * d/tests: include test_automatic_rename_option, exclude test_load_zsh_autotitle_warning

 -- Sebastien Delafond <seb@debian.org>  Wed, 27 Sep 2023 10:14:09 +0200

tmuxp (1.27.0-1) unstable; urgency=medium

  [ Sébastien Delafond ]
  * New upstream version 1.27.0
  * d/control: version-depend on recent python3-libtmux

 -- Sebastien Delafond <seb@debian.org>  Thu, 02 Feb 2023 08:42:28 +0100

tmuxp (1.19.1-1) unstable; urgency=medium

  [ Sébastien Delafond ]
  * New upstream version 1.19.1
  * d/control: version-depend on recent python3-libtmux

 -- Sebastien Delafond <seb@debian.org>  Sun, 18 Dec 2022 16:52:30 +0100

tmuxp (1.18.2-1) unstable; urgency=medium

  [ Sébastien Delafond ]
  * New upstream version 1.18.2

 -- Sebastien Delafond <seb@debian.org>  Thu, 24 Nov 2022 17:19:20 +0100

tmuxp (1.17.1-1) unstable; urgency=medium

  [ Sébastien Delafond ]
  * New upstream version 1.17.1
  * Bump-up Standards-Version
  * Bump-up dependency on python3-libtmux to 0.15.8

 -- Sebastien Delafond <seb@debian.org>  Sun, 23 Oct 2022 20:14:10 +0200

tmuxp (1.11.1-1) unstable; urgency=medium

  [ Sébastien Delafond ]
  * New upstream version 1.11.1

 -- Sebastien Delafond <seb@debian.org>  Fri, 06 May 2022 08:18:45 +0200

tmuxp (1.11.0-1) unstable; urgency=medium

  [ Sébastien Delafond ]
  * New upstream version 1.11.0
  * Bump-up Standards-Version

 -- Sebastien Delafond <seb@debian.org>  Thu, 28 Apr 2022 09:55:55 +0200

tmuxp (1.9.2-1) unstable; urgency=medium

  [ Sébastien Delafond ]
  * Clean-up dependencies
  * New upstream version 1.9.2

 -- Sebastien Delafond <seb@debian.org>  Sun, 10 Oct 2021 09:32:13 +0200

tmuxp (1.7.0-1) unstable; urgency=medium

  [ Sébastien Delafond ]
  * New upstream version 1.7.0
  * Bump-up Standards-Version
  * Use debhelper-compat
  * Update d/watch version
  * Remove unused lintian override

 -- Sebastien Delafond <seb@debian.org>  Wed, 03 Feb 2021 10:19:41 +0100

tmuxp (1.5.4-1) unstable; urgency=medium

  * Bump up Standards-Version
  * Remove python2 package
  * Remove obsolete HYPOTHESIS_DATABASE_FILE environment variable
  * New upstream version 1.5.4

 -- Sebastien Delafond <seb@debian.org>  Sat, 21 Dec 2019 10:16:50 +0100

tmuxp (1.5.0a-1) unstable; urgency=medium

  * New upstream release (Closes: #919167)

 -- Sebastien Delafond <seb@debian.org>  Wed, 16 Jan 2019 18:00:23 +0100

tmuxp (1.4.0-2) unstable; urgency=medium

  * Depend on python3-pkg-resources (Closes: #904769)

 -- Sebastien Delafond <seb@debian.org>  Sat, 04 Aug 2018 08:05:09 +0200

tmuxp (1.4.0-1) unstable; urgency=medium

  * New upstream version 1.4.0
  * Use https for Vcs-Git URL
  * Use https for homepage URL
  * Use https for copyright-format URL
  * Use http for watch URL
  * Bump-up Standards-Version
  * Change section from python to utils

 -- Sebastien Delafond <seb@debian.org>  Fri, 30 Mar 2018 13:31:15 +0200

tmuxp (1.3.5-2) unstable; urgency=medium

  * Switch to DEP 14
  * Update Vcs-* to point to salsa.d.o

 -- Sebastien Delafond <seb@debian.org>  Fri, 16 Feb 2018 17:18:27 +0100

tmuxp (1.3.5-1) unstable; urgency=medium

  [ Sébastien Delafond ]
  * New upstream version 1.3.5

 -- Sebastien Delafond <seb@debian.org>  Wed, 03 Jan 2018 11:35:24 +0100

tmuxp (1.3.4-1) unstable; urgency=medium

  * New upstream version 1.3.4
  * Bump-up Standards-Version

 -- Sebastien Delafond <seb@debian.org>  Fri, 27 Oct 2017 09:11:21 +0200

tmuxp (1.3.3-1) unstable; urgency=medium

  * New upstream version 1.3.3

 -- Sebastien Delafond <seb@debian.org>  Sat, 07 Oct 2017 10:42:19 +0200

tmuxp (1.3.1-1) unstable; urgency=medium

  * Imported Upstream version 1.3.1

 -- Sebastien Delafond <seb@debian.org>  Tue, 04 Jul 2017 14:37:24 +0200

tmuxp (1.2.6-2) unstable; urgency=medium

  * Target usntable

 -- Sebastien Delafond <seb@debian.org>  Wed, 21 Jun 2017 17:48:37 +0200

tmuxp (1.2.6-1) experimental; urgency=medium

  * Imported Upstream version 1.2.6

 -- Sebastien Delafond <seb@debian.org>  Sat, 25 Feb 2017 09:33:10 +0100

tmuxp (1.2.5-2) experimental; urgency=medium

  * Add Vcs-* fields

 -- Sebastien Delafond <seb@debian.org>  Sun, 19 Feb 2017 12:12:07 +0100

tmuxp (1.2.5-1) experimental; urgency=medium

  * NEW package (Closes: #774055)
  * Imported Upstream version 1.2.5
  * Take maintainership
  * Fix descriptions
  * Bump-up Standards-Version
  * Add copyright file

 -- Sebastien Delafond <seb@debian.org>  Sat, 18 Feb 2017 12:35:41 +0100

tmuxp (1.2.4-0ubuntu1~ubuntu17.04.1~ppa1) zesty; urgency=medium

  * bump version
  * fix deps
  * New upstream release
  * No-change backport to zesty

 -- Ivan Borzenkov <ivan1986@list.ru>  Wed, 01 Feb 2017 01:03:07 +0300

tmuxp (1.2.3-1) unstable; urgency=low

  * source package automatically created by stdeb 0.8.5

 -- Ivan Borzenkov <ivan1986@list.ru>  Sun, 01 Jan 2017 16:10:18 +0300
